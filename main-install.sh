#!/bin/bash

INVENTORY=~/.rapid_ansible_hosts 

ansible-galaxy collection install ansible.posix

echo localhost ansible_connection=local > $INVENTORY
if ansible-playbook -i $INVENTORY ./install-xfce.yaml   && \
   ansible-playbook -i $INVENTORY ./install-cadence.yaml
then
  echo "sucess cadence desktop install succeeded" >> /tmp/rapid_image_complete
else
  echo "fail  playbook failed" >> /tmp/rapid_image_complete
fi
